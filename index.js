function testService(events) {
  if (!events.length) return true;
  if (events.length % 2 !== 0) return false;

  let dataIsValid = true;
  let customers = {};

  events.forEach((event) => {
    if (event[1] === "in") {
      if (customers[event[0]]) {
        dataIsValid = false;
        return;
      } else {
        customers[event[0]] = 1;
      }
    } else {
      if (customers[event[0]]) {
        customers[event[0]] -= 1;
      } else {
        dataIsValid = false;
        return;
      }
    }
  });

  return dataIsValid;
}

module.exports = testService;
